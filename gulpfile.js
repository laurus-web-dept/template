var gulp = require('gulp');
var gutil = require('gulp-util');
var concat = require('gulp-concat');
var browserify = require('gulp-browserify');
var compass = require('gulp-compass');
var connect = require('gulp-connect');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var minifyHTML = require('gulp-minify-html');

var outputDir = 'builds/development/';
var jsSources = ['components/scripts/lib/*.js', 'components/scripts/*.js'];
var sassSources = ['components/sass/**/*.scss'];
var htmlSources = [outputDir + '*.html'];
var cssSources = [outputDir + '/css/*.css'];

gulp.task('connect', function(){
	connect.server({
		livereload: true
	});
});

gulp.task('bootstrap', function(){
	gulp.src('node_modules/bootstrap-sass/assets/fonts/bootstrap/*.*')
		.pipe(gulp.dest('components/fonts/bootstrap'))
	gulp.src('node_modules/bootstrap-sass/assets/stylesheets/**/*.*')
		.pipe(gulp.dest('components/sass/bootstrap'))
	gulp.src(['node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js', 'node_modules/jquery/dist/jquery.min.js'])
		.pipe(gulp.dest('components/scripts/lib/')) 

})

gulp.task('font-awesome', function(){
	gulp.src('node_modules/font-awesome/scss/*.*')
		.pipe(gulp.dest('components/sass/font-awesome'))
	gulp.src('node_modules/font-awesome/fonts/*.*')
		.pipe(gulp.dest('components/fonts/'))
})

gulp.task('js', function(){
	gulp.src(jsSources)
		.pipe(concat('script.js'))
		.pipe(browserify())		
		.pipe(gulp.dest(outputDir+'/js'))
		.pipe(connect.reload())
})


gulp.task('html', function(){
	gulp.src(htmlSources)
		.pipe(connect.reload())
});

gulp.task('sass', function(){
	gulp.src('components/sass/style.scss')
	.pipe(compass({
		sass: 'components/sass/',
		css: 'builds/development/css',
		image: 'builds/development/images',
		style: 'expanded'	
	}))
	.pipe(connect.reload())
	.on('error', function(err){ console.log(err.message)})
	.pipe(gulp.dest('builds/development/css'))
	
})

gulp.task('fonts', function(){
	gulp.src(['components/fonts/**/*.*'],{
		base: 'components/fonts/'
	})
	.pipe(gulp.dest('builds/development/fonts'))
});


gulp.task('watch', function(){
	gulp.watch(jsSources, ['js']);
	gulp.watch(htmlSources, ['html']);
	gulp.watch(sassSources, ['sass']);
	gulp.watch('components/fonts/**/*.*', ['fonts']);
});

gulp.task('prod', function(){
	var outputDir = 'builds/production';
	gulp.src(jsSources)
		.pipe(concat('script.js'))
		.pipe(uglify('script.js'))
		.pipe(gulp.dest(outputDir + '/js'))
	gulp.src(htmlSources)
		.pipe(minifyHTML({conditionals: true}))
		.pipe(gulp.dest(outputDir))
	gulp.src(cssSources)
		.pipe(concat('style.css'))
		.pipe(uglifycss('style.css'))		
		.pipe(gulp.dest(outputDir + '/css'))
});

gulp.task('default', ['html', 'js', 'sass', 'fonts', 'watch', 'connect']);