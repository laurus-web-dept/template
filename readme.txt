The Official Laurus College Web Department Template

Using the Template
1) Create a new folder for your site
2) Open Git Bash and navigate to the new folder
3) Clone the Laurus Workflow Template
   $ git clone https://bitbucket.org/laurus-web-dept/template.git .
4) Install gulp dependencies - $ npm install
5) Run gulp - $ gulp

Add Bootstrap
1) Run - $ gulp bootstrap
2) Add at the top of style.scss - @import 'bootstrap/_bootstrap.scss';

Add Font Awesome
1) Run - $ gulp font-awesome
2) Add at the top of style.scss - @import 'font-awesome/font-awesome.scss';

Create Development Build
1) Create website in the builds/development folders
2) Create js in the components/script folder
3) Create scss in the components/sass folder
4) View website at localhost:8080/builds/development

Create Production Build
1) Run - $ gulp prod

*****************************
Installing Necessary Software
*****************************
Windows Install
1) Install Git - https://git-scm.com/download/win
	Set the following options
		Windows Explorer Integration > Simple Context Menu
		Use Git from Git Bash Only
		Checkout Windows-style
2) Install NodeJS - https://nodejs.org/
3) Install Ruby - http://rubyinstaller.org/downloads/
	Set the following options
		Add ruby to PATH
4) Run Git Bash from the start menu
5) Check to see if everything is installed correctly
	$ node -v
	$ gem -v
6) Use Ruby to install sass & compass
	$ gem install sass
	$ gem install copmass	
7) Check to see if sass/compass installed correctly
	$ sass -v
	$ compass -v
	
Mac Install
Mac Note: some commands may require you to add sudo ($ sudo gem install sass)
1) Install Xcode
	Visit the Mac App Store and install Xcode
	Open applications > Utilities > Terminal
	$ xcode-select --install
	Click Install on the prompt
	Enter your computer account password
	Accept the Xcode license
2) Install Homebrew	
	Open Terminal
	$ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	Once installed: $ brew doctor
3) Install Git
	Open Terminal
	$ brew install git
4) Install Node.js
	Open Terminal
	$ brew install node
	$ npm version
5) Use Ruby to install sass & compass	
	Note: Mac already should have ruby installed automatically
	$ gem install sass
	$ gem install compass
6) Check to see if sass/compass installed correctly
	$ sass -v
	$ compass -v

http://burnedpixel.com/blog/beginners-setup-guide-for-ruby-node-git-github-on-your-mac/






